def checkBin(bin):
    pom = str(bin)
    #print(bin)
    dlug = len(pom)
    
    for i in range(0,dlug):
        #print(pom[i])
        spraw = int(pom[i])
        if (spraw != 0) and (spraw !=1):
            return False
    return True        
        
def numberZero(bin):
    pom = str(bin)
    dlug = len(pom)
    sumaZer = 0
    
    for i in range(0,dlug):
        #print(pom[i])
        spraw = int(pom[i])
        if (spraw == 0):
            sumaZer += 1
    return sumaZer

def numberOne(bin):
    pom = str(bin)
    dlug = len(pom)
    sumaJed = 0
    
    for i in range(0,dlug):
        #print(pom[i])
        spraw = int(pom[i])
        if (spraw == 1):
            sumaJed += 1
    return sumaJed

def theSameNumberBin(bin):
    if (numberZero(bin) == numberOne(bin)):
        return True
    else:
        return False

def testCzek():
    assert(checkBin(binarka)==True)

def testZero():
    assert(numberZero(binarka)==3)    

def testOne():
    assert(numberOne(binarka)==5)    

def testSame():
    assert(theSameNumberBin(binarka)==False)

binarka = 11010101 
print(checkBin(binarka))   
