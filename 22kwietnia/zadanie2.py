class Item:
    __name = "Unknown"
    __weight = 0 
    def __init__(self):
        __name = "Unknown"
        __weight = 0
    def getName(self):
        return self.__name
    
    def getWeight(self):
        return self.__weight
    
    def setName(self, name):
        self.__name = name

    def setWeight(self, weight):
        self.__weight = weight
    
    def __str__(self):
        return f"Name: {self.__name}, weight: {self.__weight}"   

class Juice(Item):
    __volume = 0
    __price = 0
    def __init__(self):
        __volume = 0
        __price = 0
        #super().__init__()
        #super().__name = "Unknown Juice"

    def getVolume(self):
        return self.__volume
    
    def setVolume(self, volume):
        self.__volume = volume

    def __str__(self):
        return f"Name: {self.__name}, weight: {self.__weight}, volume: {self.__volume}, price: {self.__price}"   

class Shoes(Item):
    __size = 0
    def __init__(self):
        __size = 0

    def getSize(self):
        return self.__size
    
    def setSize(self, size):
        self.__size = size

class Fruit(Item):
    __type = 0
    def __init__(self):
        __type = 0

    def getType(self):
        return self.__type
    
    def setType(self, type):
        self.__type = type

item1 = Item() 

print(item1)

juice1 = Juice()

print(juice1)
