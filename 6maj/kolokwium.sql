--DROP
--DROP TABLE przedmiot;
--DROP TABLE konto;
--DROP TABLE pracownik;
--DROP TABLE student;
--DROP TABLE osoba;
--CREATE
CREATE TABLE osoba(
id serial,
imie varchar(16),
nazwisko varchar(32),
CONSTRAINT osoba_pk PRIMARY KEY(id)
);

CREATE TABLE student(
id serial,
nr_indeksu char(8),
dane integer UNIQUE,
CONSTRAINT student_pk PRIMARY KEY(id),
CONSTRAINT student_fk FOREIGN KEY(dane) REFERENCES osoba(id)
);

CREATE TABLE pracownik(
id serial,
nr_pracownika char(8),
dane integer UNIQUE,
typ varchar(32),
CONSTRAINT pracownik_pk PRIMARY KEY(id),
CONSTRAINT pracownik_fk FOREIGN KEY(dane) REFERENCES osoba(id)
);

CREATE TABLE konto(
id serial,
login varchar(32) NOT NULL,
haslo varchar(16) NOT NULL,
email varchar(32) NOT NULL,
ostatnia_zmiana_hasla date,
wlasciciel integer,
zmiana_hasla boolean,
CONSTRAINT konto_pk PRIMARY KEY(id),
CONSTRAINT konto_fk FOREIGN KEY(wlasciciel) REFERENCES osoba(id)
);

CREATE TABLE przedmiot(
id serial,
nazwa varchar(32),
nauczyciel integer,
CONSTRAINT przedmiot_pk PRIMARY KEY(id),
CONSTRAINT przedmiot_fk FOREIGN KEY(nauczyciel) REFERENCES pracownik(id)
);
--INSERT OSOBA
INSERT INTO osoba(imie,nazwisko) VALUES ('Jan','Kowalski');
INSERT INTO osoba(imie,nazwisko) VALUES ('Adam','Nowak');
INSERT INTO osoba(imie,nazwisko) VALUES ('Joanna','Wiśniewska');
INSERT INTO osoba(imie,nazwisko) VALUES ('Robert','Wójcik');
INSERT INTO osoba(imie,nazwisko) VALUES ('Mirosława','Kowalczyk');
INSERT INTO osoba(imie,nazwisko) VALUES ('Piotr','Kamiński');
INSERT INTO osoba(imie,nazwisko) VALUES ('Marta','Lewandowska');
INSERT INTO osoba(imie,nazwisko) VALUES ('Szymon','Zieliński');
INSERT INTO osoba(imie,nazwisko) VALUES ('Jan','Szymański');
INSERT INTO osoba(imie,nazwisko) VALUES ('Jan','Kowalski');
INSERT INTO osoba(imie,nazwisko) VALUES ('Kamil','Wójcik');
INSERT INTO osoba(imie,nazwisko) VALUES ('Anna','Kowalska');
--INSERT STUDENT
INSERT INTO student(nr_indeksu,dane) VALUES ('123456',1);
INSERT INTO student(nr_indeksu,dane) VALUES ('234567',2);
INSERT INTO student(nr_indeksu,dane) VALUES ('345678',3);
INSERT INTO student(nr_indeksu,dane) VALUES ('456789',4);
INSERT INTO student(nr_indeksu,dane) VALUES ('567890',5);
INSERT INTO student(nr_indeksu,dane) VALUES ('678901',6);
INSERT INTO student(nr_indeksu,dane) VALUES ('789012',7);
INSERT INTO student(nr_indeksu,dane) VALUES ('789012',11);
INSERT INTO student(dane) VALUES (12);
--INSERT PRACOWNIK
INSERT INTO pracownik(dane,nr_pracownika,typ) VALUES (8,'p12345','dydaktyczny');
INSERT INTO pracownik(dane,nr_pracownika,typ) VALUES (9,'p23456','dydaktyczny');
INSERT INTO pracownik(dane,nr_pracownika,typ) VALUES (10,'pa1234','administracyjny');
--INSERT KONTO
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('jkowalski','zaq1','janek@wp.pl','2023-03-03',1,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('anowak','haslo','adam@op.pl','2023-02-23',2,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('awisnewska','password','awisnewska@gmail.com','2022-10-04',3,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('rwojcik','123123','robertw@wp.pl','2022-12-23',4,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('mkowalczyk','mkowalczyk','mir@o2.pl','2023-04-13',5,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('pkaminski','rwxghu','pk@wp.pl','2023-05-01',6,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('mlewandowska','drowssap','marlew@gmail.com','2022-12-31',7,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('szzielinski','hd6e8dhs','szzielinski@ug.edu.pl','2023-04-15',8,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('jaszymanski','hhgs359sff','jaszymanski@ug.edu.pl','2023-05-01',9,FALSE);
INSERT INTO konto(login,haslo,email,ostatnia_zmiana_hasla,wlasciciel,zmiana_hasla)
	VALUES ('jakowalski','54efghj5g','jakowalski@ug.edu.pl','2022-03-03',10,FALSE);
INSERT INTO konto(login,haslo,email)
	VALUES ('test','test','test@ug.edu.pl');
INSERT INTO konto(login,haslo,email)
	VALUES ('admin','zaq1','admin@ug.edu.pl');
--INSERT PRZEDMIOT
INSERT INTO przedmiot(nazwa,nauczyciel) VALUES ('Bazy danych',1);
INSERT INTO przedmiot(nazwa,nauczyciel) VALUES ('Języki programowania',1);
INSERT INTO przedmiot(nazwa,nauczyciel) VALUES ('Środowisko programisty',2);

--Zadania

--Zad 1
--Dodaj do bazy możliwość zapisywania studentow na przedmioty,
--dodana relacja wiele do wielu (pomiędzy studentem i przedmiotem) powinna pozwalać na wystawienie oceny
--zapisz dwóch studentów na wszystkie przedmioty bez wystawiania ocen



--Zad 2
--Z tabeli osoba wypisz nazwiska kończące się literą a



--Zad 3
--Z tabeli konto wypisz loginy i liczbę dni od ostaniej zmiany hasła



--Zad 4 	
--Wypisz numery pracowników oraz ich loginy



--Zad 5
--Ustaw wartość zmiana_hasla na TRUE osób, które nie zmieniły hasła w ciągu ostatnich 30 dni



--Zad 6
--Wypisz konta nieposiadające właścicieli



--Zad 7
--Napisz zapytanie które sprawdzi czy hasło jest unikalne
--loginy kont z tym samym hasłem



--Zad 8
--Wypisz przedmioty nie prowadzone przez pracownika Szymon Zieliński



--Zad 9
--Wypisz przedmioty razem z nazwiskiem i adresem email nauczyciela



---Zad 10
--Napisz polecenie które wymusi aby każdy wiersz z tabeli student
--odwoływał się do dokładnie jednego wiersza w tabeli osoba (ALTER TABLE)