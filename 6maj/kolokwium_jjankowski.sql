--Zadania

--Zad 1
--Dodaj do bazy możliwość zapisywania studentow na przedmioty,
--dodana relacja wiele do wielu (pomiędzy studentem i przedmiotem) powinna pozwalać na wystawienie oceny
--zapisz dwóch studentów na wszystkie przedmioty bez wystawiania ocen

CREATE TABLE zapisy(
id serial,
id_studenta serial,
id_przedmiotu serial,
CONSTRAINT zapisy_pk PRIMARY KEY(id),
CONSTRAINT zapisy_fk_student FOREIGN KEY(id_studenta) REFERENCES student(id),
CONSTRAINT zapisy_fk_przedmiot FOREIGN KEY(id_przedmiotu) REFERENCES przedmiot(id)
);

INSERT INTO zapisy(id_studenta,id_przedmiotu) VALUES (1,1);
INSERT INTO zapisy(id_studenta,id_przedmiotu) VALUES (1,2);
INSERT INTO zapisy(id_studenta,id_przedmiotu) VALUES (1,3);

INSERT INTO zapisy(id_studenta,id_przedmiotu) VALUES (2,1);
INSERT INTO zapisy(id_studenta,id_przedmiotu) VALUES (2,2);
INSERT INTO zapisy(id_studenta,id_przedmiotu) VALUES (2,3);

--Zad 2
--Z tabeli osoba wypisz nazwiska kończące się literą a

SELECT nazwisko FROM osoba WHERE nazwisko LIKE '%a'

--Zad 3
--Z tabeli konto wypisz loginy i liczbę dni od ostaniej zmiany hasła

SELECT login, ('2023-05-20'- ostatnia_zmiana_hasla) AS dni_od_zmian_hasla FROM konto

--Zad 4 	
--Wypisz numery pracowników oraz ich loginy

SELECT login, nr_pracownika FROM pracownik INNER JOIN konto ON konto.wlasciciel = pracownik.dane 

--Zad 5
--Ustaw wartość zmiana_hasla na TRUE osób, które nie zmieniły hasła w ciągu ostatnich 30 dni

UPDATE konto SET zmiana_hasla = TRUE WHERE ('2023-05-20'- ostatnia_zmiana_hasla)>30

--Zad 6
--Wypisz konta nieposiadające właścicieli

SELECT * FROM konto WHERE wlasciciel IS NULL

--Zad 7
--Napisz zapytanie które sprawdzi czy hasło jest unikalne
--loginy kont z tym samym hasłem

SELECT login FROM konto WHERE haslo IN (SELECT haslo FROM konto GROUP BY haslo HAVING COUNT(*)>1)

--Zad 8
--Wypisz przedmioty nie prowadzone przez pracownika Szymon Zieliński

SELECT przedmiot.nazwa FROM przedmiot INNER JOIN pracownik ON przedmiot.nauczyciel=pracownik.id INNER JOIN osoba ON pracownik.dane = osoba.id WHERE osoba.imie !='Szymon' AND osoba.nazwisko !='Zieliński'

--Zad 9
--Wypisz przedmioty razem z nazwiskiem i adresem email nauczyciela

SELECT przedmiot.nazwa, osoba.nazwisko, konto.email FROM przedmiot INNER JOIN pracownik ON przedmiot.nauczyciel = pracownik.id INNER JOIN osoba ON pracownik.dane = osoba.id INNER JOIN konto ON osoba.id = konto.wlasciciel  

---Zad 10
--Napisz polecenie które wymusi aby każdy wiersz z tabeli student
--odwoływał się do dokładnie jednego wiersza w tabeli osoba (ALTER TABLE)

