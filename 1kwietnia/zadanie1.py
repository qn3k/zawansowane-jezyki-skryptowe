oceny = {"niedostateczny": 1,
         "dopuszczajacy": 2,
         "dostateczny": 3,
         "dobry": 4,
         "bardzo dobry": 5,
         "celujacy": 6}

print(oceny)

def ug():
    del oceny["dopuszczajacy"]
    #oceny.pop("dopuszczajacy")
    oceny.pop("celujacy")
    oceny["niedostateczny"] = 2
    oceny["dostateczny plus"] = 3.5
    oceny["dobry plus"] = 4.5
    return True

def testug():
    assert(ug()==True)

ug()
print(oceny)