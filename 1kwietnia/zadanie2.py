a = {"re":3,"im":2}
b = {"re":5,"im":4}

def testSuma():
    assert(suma(a,b)=={"re":8, "im":6})

def testRoznica():
    assert(roznica(a, b) == {"re": -2, "im": -2})

def testMnozenie():
    assert(mnozenie(a, b) == {"re": 7, "im": 22})

def testDzielenie():
    assert (dzielenie(a, b) == {"re": 15, "im": 10})

def suma(a,b):
    suma_re = a["re"] + b["re"]
    suma_im = a["im"] + b["im"]
    '''print("Suma to: ",end="")
    print(suma_re,end="+")
    print(suma_im,end="i\n")'''
    suma_cal = {"re":suma_re,"im":suma_im}
    return suma_cal


def roznica(a,b):
    roznica_re = a["re"] - b["re"]
    roznica_im = a["im"] - b["im"]
    '''print("Roznica to: ", end="")
    if (roznica_im>0):
        print(roznica_re,end="+")
    else:
        print(roznica_re, end="")
    print(roznica_im,end="i\n")'''
    roznica_cal = {"re":roznica_re,"im":roznica_im}
    return roznica_cal

def mnozenie(a,b):
    mnozenie_re = (a["re"] * b["re"])-(a["im"] * b["im"])
    mnozenie_im = (a["im"] * b["re"])+(a["re"] * b["im"])
    '''print("Iloczyn to: ", end="")
    if (mnozenie_im>0):
        print(mnozenie_re,end="+")
    else:
        print(mnozenie_re, end="")
    print(mnozenie_im,end="i\n")'''
    mnozenie_cal = {"re":mnozenie_re,"im":mnozenie_im}
    return mnozenie_cal

def dzielenie(a,b):
    mianownik = (b["re"]**2)+(b["im"]**2)
    licznik_re = ((a["re"] * b["re"])+(a["im"] * b["im"])/mianownik)
    licznik_im = ((a["im"] * b["re"])-(a["re"] * b["im"])/mianownik)
    '''if (licznik_im>0):
        print("(",licznik_re, sep="", end="+")
    else:
        print("(",licznik_re, sep="", end="")
    print(licznik_im,end="i)/")
    print(mianownik)'''
    dzielenie_cal = {"re":round(licznik_re,2),"im":round(licznik_im,2)}
    return dzielenie_cal

print("Suma to:", suma(a,b))
print("Roznica to:", roznica(a,b))
print("Iloczyn to:", mnozenie(a,b))
print("Iloraz to:", dzielenie(a,b))



